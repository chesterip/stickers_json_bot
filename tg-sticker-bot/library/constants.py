from enum import Enum


class ConversationState(Enum):
    """Conversation state of a user."""
    BROADCAST = 0
    CONFIG_SELECT = 1
    CONFIG_EDIT = 2
    CUSTOMPACK_SETNAME = 3
    CUSTOMPACK_ADDSTICKER = 4
    GETURL_WAITFORSTICKER = 5


class Text():
    HELP_MESSAGE = """**Convert stickers into JSON**
Send the URL of LINE/Telegram/Kakao stickers, such as:
- https://store.line.me/stickershop/product/12541/zh-Hant
- https://t.me/addstickers/FreddysNightmares
- https://e.kakao.com/t/sweet-and-funny-honey-bee
or LINE stickers ID, such as:
- 12541
or /custompack to create your own sticker pack based on Telegram sticker
or /popular to list top 5 popular sticker sets
or /geturl to get the URL of a Telegram sticker set
or /help to display this help message"""

    ADMIN_HELP_MESSAGE = """**Admin commands**
/broadcast - send message to all users
/buildprev `$FILENAME` - build preview for a specific sticker set
/buildprev `all` - rebuild preview for all sticker sets in cache
/buildprev `fix` - build preview for sticker sets without a preview image in cache
/cache - list the cache
/cache `fix` - remove cached stickers with no preview file
/config - open config console
/log - get the simplified log
/log `full` - get the complete log
/r - restart the server at OS level"""

    NOT_ENOUGH_STICKERS = """**Process aborted.**\n
Image set too small to work with WhatsApp.
At least 3 stickers are required."""

    N_STICKERS_COLLECTED = "[%s] %s stickers have been collected for custom pack by user %s"

    CUSTOM_PACK_GEN_JSON = "[%s] began generation of custom pack for user %s"