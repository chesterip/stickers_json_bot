# Author: @leotang1993
# This file holds all global stuff.

import os
import re

import ujson as json

from library.metaclass import Singleton


# Class for managing all interaction with config and chats
class SettingsManager(metaclass=Singleton):
    _chats_modified = False     # write_chats will only save if chats is modified
    _immutable_config_arr = ["token", "admin"]   # Hard-code immutable config keys

    def __init__(self):
        with open('settings/config.json') as config_file:
            self._config = json.load(config_file)

        # Create chats JSON file if not exists
        if not os.path.isfile("settings/chats.json"):
            with open("settings/chats.json", 'w') as chats_file:
                json.dump({}, chats_file)
                self._chats = {}
        else:   # Load chats JSON file
            with open('settings/chats.json') as chats_file:
                self._chats = json.load(chats_file)

    # Return value of specific config using key
    def get_config(self, config_key):
        return self._config[config_key]

    # Return if user is admin
    def is_admin(self, username):
        return username in self._config["admin"]

    # Decorator for admin-only commands
    def admin_only(self, func):
        def __wrapped__(update, context, **kwargs):
            if self.is_admin(update.message.from_user.username):
                return func(update, context, **kwargs)
            else:
                return None

        return __wrapped__

    # Return all editable config
    def get_editable_config(self):
        # Load config file into edit_config_dict
        with open('settings/config.json') as config_file:
            edit_config_dict = json.load(config_file)

        # Removed immutable config options from edit_config_dict
        for key in self._immutable_config_arr:
            del edit_config_dict[key]

        return edit_config_dict

    # Edit config by key, then write entire config object into config.json
    def edit_config(self, key, value):
        # Get original value for verification and output
        original_value = self._config[key]
        assign_value = value
        if type(original_value) is int:
            assign_value = int(assign_value)
        elif type(original_value) is float:
            assign_value = float(assign_value)
        elif type(original_value) is list:
            assign_value = re.split(r'[\n\r]+', assign_value)

        # Change config variable and write to config.json
        self._config[key] = assign_value
        with open("settings/config.json", "w", encoding="utf-8") as config_file:
            json.dump(self._config, config_file, ensure_ascii=False, indent=4)

    # Reload config from config.json
    def reload_config(self):
        with open('settings/config.json') as config_file:
            self._config = json.load(config_file)

    # write chats into chats.json
    def write_chats(self, force_save=False):
        if force_save or self._chats_modified:
            with open("settings/chats.json", "w", encoding="utf-8") as chats_file:
                json.dump(self._chats, chats_file, ensure_ascii=False)
            self._chats_modified = False

    # Add new chat into chats
    def add_chat(self, chat_id):
        if str(chat_id) not in self._chats:
            self._chats[str(chat_id)] = 0

    # Remove deleted chat from chats
    def remove_chat(self, chat_id):
        if str(chat_id) in self._chats:
            del self._chats[str(chat_id)]

    # Return user's latest query time, used for cooldown mechanic
    def get_chat_last_used(self, chat_id):
        last_used = 0
        if str(chat_id) in self._chats:
            last_used = self._chats[str(chat_id)]
        return last_used

    # Set user's latest query time, used for cooldown mechanic
    def set_chat_last_used(self, chat_id, time):
        self._chats_modified = True
        self._chats[str(chat_id)] = time

    # Return all chat id initiated with bot
    def get_all_chats(self):
        return self._chats.keys()
