#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Version 0.1
# Author: @chesterip
"""Generate preview files for ALL sticket sets in cache
"""

from glob import glob

from modules.utils.image_process import preview_generate
import ujson as json

for sticker_set in glob("cache/*.json"):
    with open(sticker_set, encoding="utf-8") as data_file:
        data = [sticker_set, json.load(data_file)]
    preview_generate(data)
