# Author: @rennalol

import os
import logging
import zipfile

import ujson as json

from modules.db import DBManagers
from modules.utils.image_process import popular_preview_generate

logger = logging.getLogger(__name__)
dbm = DBManagers()


def generate_popular_preview():
    """Generate popular.webp to cache folder"""

    query = []
    popular_message = ""

    for sticker_set in dbm.stickers.find().sort("request_count", -1):
        if len(query) > 5:
            break
        archive_found = False

        source = sticker_set['source']
        target = sticker_set['target']

        filename = os.path.join('cache', source[14:], target + '_1.json')

        # Check if sticker got archived.
        if not os.path.isfile(filename) and os.path.isfile(filename + ".zip"):
            with zipfile.ZipFile(filename + ".zip", 'r') as zip_ref:
                zip_ref.extractall('.')
            os.remove(f"cache/{filename}.zip")

        # Read corresponding json file into query
        if os.path.isfile(filename):
            with open(filename, encoding="utf-8") as json_file:
                query.append((filename, json.load(json_file), source))

        # Get each set's url
        if source in dbm.sites:
            popular_message += dbm.sites[source].get_url(target) + "\n"

    # Generate popular preview using image_process module
    popular_preview_generate(query)

    # Put popular_message into cache/popular/popular.json
    with open("cache/popular/popular.txt", "w") as text_file:
        text_file.write(popular_message)


def popular_stickers(update, context):
    """/popular - List top five most popular sticker sets"""
    chat_id = update.message.chat_id
    user = update.message.from_user
    logger.info("[%s] User %s issued %s", chat_id,
                user.username, update.message.text)

    # Check if popular preview is generated
    if (
        not os.path.isfile("cache/popular/popular.json") and
        os.path.isfile("cache/popular/popular.webp")
    ):
        generate_popular_preview()

    # Send popular preview
    context.bot.send_photo(
        chat_id=chat_id,
        photo=open("cache/popular/popular.webp", 'rb')
    )

    # Read list of popular stickers from popular.txt
    with open("cache/popular/popular.txt", "r") as text_file:
        popular_message = text_file.read()

    # Display the list of popular stickers
    context.bot.send_message(
        chat_id=chat_id, text=popular_message,
        disable_web_page_preview=True,
        parse_mode="HTML")
