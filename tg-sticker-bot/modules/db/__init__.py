from datetime import datetime
from pymongo import MongoClient

from library.metaclass import Singleton
import logging

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)


class DBManagers(metaclass=Singleton):

    def __init__(self):
        self.client = MongoClient('db', 27017)
        self.db = self.client.stickers_bot
        self.sites = {}

    @property
    def users(self):
        return self.db.users

    @property
    def stickers(self):
        return self.db.stickers

    @property
    def log(self):
        return self.db.log

    def touch_sticker(self, target, source):
        return self.stickers.update_one(
            {'target': target, 'source': source},
            {
                '$set': {'last_request': datetime.now().timestamp()},
                '$inc': {'request_count': 1}
            },
            upsert=True,
        )

    def touch_tg_sticker(self, file_id, sticker_set):
        return self.stickers.update_one(
            {'file_id': file_id, 'sticker_set': sticker_set},
            {
                '$set': {'last_request': datetime.now().timestamp()},
                '$inc': {'request_count': 1}
            },
            upsert=True,
        )

    def touch_user(self, chat_id):
        return self.users.update_one(
            {'chat_id': chat_id},
            {
                '$set': {'last_request': datetime.now().timestamp()},
                '$inc': {'request_count': 1}
            },
            upsert=True,
        )

    def error_log(self, update, context):
        logger = logging.getLogger(__name__)
        logger.warning('Update "%s" caused error "%s"', update, context.error)
        self.log.insert_one({
            "date": update.message.date,
            "type": update.message.entities[0].type if update.message.entities else "message",
            "module": "ERROR",
            "text": update.message.text,
            "username": update.message.from_user.username,
            "user_id": update.message.from_user.id,
            "error": context.error,
        })

    def logged(self, arg):
        """
        Using this decorator to write log to db and stdout
        @logged can be used with or without passing __name__ as arg
        """
        # if the arg is a function, which means __name__ is not passed here
        logger = logging.getLogger(__name__ if callable(arg) else arg)

        def _logged(func):
            def __wrapped__(update, context, **kwargs):
                logger.info(
                    f"[{update.message.from_user.id}] User {update.message.from_user.username} sent {update.message.text}")
                self.log.insert_one({
                    "date": update.message.date,
                    "type": update.message.entities[0].type if update.message.entities else "message",
                    "module": __name__ if callable(arg) else arg,
                    "text": update.message.text,
                    "username": update.message.from_user.username,
                    "user_id": update.message.from_user.id,
                })
                return func(update, context, **kwargs)

            return __wrapped__

        if callable(arg):
            return _logged(arg)

        return _logged

    def register_site(self, arg):
        """
        Using this decorator to register site class
        """
        site = arg

        def wrapped(cls):
            self.sites[site] = cls
            return cls

        return wrapped
