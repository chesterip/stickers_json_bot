import logging
import os

import shutil
from uuid import uuid4

from telegram.ext import ConversationHandler

from library.constants import ConversationState, Text
from modules.db import DBManagers
from modules.utils import generate_json
from modules.utils.image_process import preview_generate
import settings.settings as settings

logger = logging.getLogger(__name__)

settings_manager = settings.SettingsManager()
dbm = DBManagers()


@dbm.logged(__name__)
def start(update, context):
    """Start /custompack conversation
    Ask for the custom pack's name"""
    user = update.message.from_user
    chat_id = update.message.chat_id
    dbm.touch_user(chat_id)

    # Display message to ask for the name of sticker pack being created
    update.message.reply_text(
        "Create custom pack.\n"
        "Please type in the name for the sticker pack.\n"
        "Type /cancel to cancel.",
        parse_mode="HTML")

    return ConversationState.CUSTOMPACK_SETNAME


@dbm.logged(__name__)
def set_name(update, context):
    """Set the sticker pack's name"""
    user = update.message.from_user
    chat_id = update.message.chat_id
    logger.info("[%s] User %s named the sticker pack as %s", chat_id,
                user.username, update.message.text)

    update.message.reply_text(
        "Please send me stickers",
        parse_mode="HTML"
    )

    # initialize sticker set object
    context.user_data["custompack"] = {
        "metadata": {
            "identifier": f"{chat_id}-{uuid4()}"[:16],
            "name": update.message.text,
            "publisher": "TG Sticker Bot"
        },
        "stickers_list": [],
        "filename": os.path.join(
            "temp",
            "custompack",
            context.user_data["custompack"]["metadata"]["identifier"]
        )
    }

    # create temporary directory
    os.makedirs(context.user_data["custompack"]["filename"])

    return ConversationState.CUSTOMPACK_ADDSTICKER


@dbm.logged(__name__)
def add_sticker(update, context):
    """Download the sticker"""
    user = update.message.from_user
    chat_id = update.message.chat_id
    sticker = update.message.sticker
    sticker_pack = context.user_data["custompack"]

    dbm.touch_tg_sticker(sticker.file_id, sticker.set_name)

    # download the sticker from Telegram
    sticker_pack["stickers_list"].append(
        sticker.get_file().download(
            custom_path=os.path.join(
                sticker_pack["filename"], sticker.file_id + ".webp"
            )
        )
    )

    logger.info("[%s] Telegram file \"%s\" downloaded for user %s",
                chat_id, sticker.file_id, user.username)

    # if there are already 30 stickers, call the finish
    if len(sticker_pack["stickers_list"]) == 30:
        update.message.reply_text(
            "Sticker limit <b>(30)</b> reached.",
            parse_mode="HTML")

        return finish(update, context)

    # otherwise, ask for more stickers if any
    update.message.reply_text(
        "You can send me more sticker, \n or type /finish to complete the set.",
        parse_mode="HTML")

    return ConversationState.CUSTOMPACK_ADDSTICKER


@dbm.logged(__name__)
def cleanup(update, context):
    """Clean-up the temporary directory and files
    when cancelled, finished or error occured"""

    filename = context.user_data["custompack"]["filename"]

    # check if the json file/ preview image/ directory exists and remove
    checklist = ("_1.json", "_1.json.webp", "")
    for item in checklist:
        if os.path.isfile(filename + item):
            os.remove(filename + item)
        elif os.path.isdir(filename + item):
            shutil.rmtree(filename)

    # erase the user data
    context.user_data["custompack"] = {}


@dbm.logged(__name__)
def finish(update, context):
    """Send the json file and preview image"""

    user = update.message.from_user
    chat_id = update.message.chat_id
    sticker_pack = context.user_data["custompack"]

    # Abort process if sticker set is too small for WhatsApp to work
    logger.info(
        Text.N_STICKERS_COLLECTED,
        chat_id, len(sticker_pack["stickers_list"]), user.username
    )

    if len(sticker_pack["stickers_list"]) < 3:
        update.message.reply_text(Text.NOT_ENOUGH_STICKERS,
                                  parse_mode="HTML")

        # Remove image folder before aborting
        shutil.rmtree(os.path.basename(sticker_pack["filename"]))

        return ConversationHandler.END    # Exit point

    else:
        # Generate JSON file
        logger.info(Text.CUSTOM_PACK_GEN_JSON, chat_id, user.username)
        update.message.reply_text('Generating JSON')
        sticker_pack["tray_path"] = sticker_pack["stickers_list"][0]
        result = generate_json(sticker_pack)[0]

    # Remove image folder
    shutil.rmtree(sticker_pack["filename"])

    # Generate preview image
    try:
        update.message.reply_text('Generating preview')
        preview_generate(result, logger=logger, custompack=True)

        # Sending Preview
        context.bot.send_photo(
            chat_id=chat_id,
            photo=open(
                sticker_pack["filename"] + '_1.json.webp',
                'rb'
            )
        )
    except Exception as ex:
        logger.warning("Preview cannot be generated nor sent.")

    # JSON file ready
    update.message.reply_text(
        'Now sending {} ({:7.2f} KiB)'.format(
            os.path.basename(sticker_pack["filename"]),
            os.stat(sticker_pack["filename"] + '_1.json').st_size / 1024
        )
    )

    # Sending JSON file
    context.bot.send_document(
        chat_id=chat_id,
        document=open(sticker_pack["filename"] + '_1.json', 'rb')
    )

    logger.info("[%s] User %s received %s", chat_id,
                user.username, sticker_pack["filename"])

    # Clean up the files
    cleanup(update, context)

    return ConversationHandler.END


@dbm.logged(__name__)
def cancel(update, context):
    """Cancel custom pack conversation"""
    user = update.message.from_user
    chat_id = update.message.chat_id

    update.message.reply_text("Create custom pack cancelled.")
    logger.info("[%s] User %s cancelled custom pack", chat_id,
                user.username)

    if "custompack" in context.user_data:
        # clean up the files
        cleanup(update, context)

    return ConversationHandler.END
