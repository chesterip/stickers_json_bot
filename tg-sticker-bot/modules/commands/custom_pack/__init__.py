from telegram.ext import (
    CallbackQueryHandler, CommandHandler, Filters,
    ConversationHandler, MessageHandler
)
from library.constants import ConversationState
from . import custom_pack

# Declare conversation handler for /custompack
handler = ConversationHandler(
    entry_points=[CommandHandler("custompack", custom_pack.start)],
    states={
        ConversationState.CUSTOMPACK_SETNAME: [
            MessageHandler(
                Filters.text,
                custom_pack.set_name
            )
        ],
        ConversationState.CUSTOMPACK_ADDSTICKER: [
            MessageHandler(
                Filters.sticker,
                custom_pack.add_sticker
            ),
            CommandHandler(
                "finish",
                custom_pack.finish
            )
        ]
    },
    fallbacks=[
        CommandHandler("cancel", custom_pack.cancel)
    ]
)