import ujson as json
from telegram.ext import ConversationHandler

import settings.settings as settings
from library.constants import ConversationState, Text
from modules.db import DBManagers
from modules.usage_stats import get_stickers_usage

settings_manager = settings.SettingsManager()
dbm = DBManagers()


@dbm.logged(__name__)
def start(update, context):
    """Start /geturl conversation
    Ask for the sticker"""
    user = update.message.from_user
    chat_id = update.message.chat_id

    dbm.touch_user(chat_id)

    # Display message to ask for the name of sticker pack being created
    update.message.reply_text(
        "Please send me a sticker from the stickers set that you are interested in.\n"
        "Type /cancel to cancel.",
        parse_mode="HTML")

    return ConversationState.GETURL_WAITFORSTICKER


@dbm.logged(__name__)
def sticker(update, context):
    """Return the sticker set URL"""
    user = update.message.from_user
    chat_id = update.message.chat_id
    set_url = "https://t.me/addstickers/" + update.message.sticker.set_name

    update.message.reply_text(set_url, parse_mode="HTML")

    return ConversationHandler.END


@dbm.logged(__name__)
def cancel(update, context):
    """Cancel get_url conversation"""
    user = update.message.from_user
    chat_id = update.message.chat_id

    update.message.reply_text("/geturl cancelled.")

    return ConversationHandler.END
