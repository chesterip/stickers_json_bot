from telegram.ext import (CommandHandler, ConversationHandler, Filters,
                          MessageHandler)

from library.constants import ConversationState

from . import get_url

# Declare conversation handler for /geturl
handler = ConversationHandler(
    entry_points=[CommandHandler("geturl", get_url.start)],
    states={
        ConversationState.GETURL_WAITFORSTICKER: [
            MessageHandler(Filters.sticker, get_url.sticker)
        ]
    },
    fallbacks=[CommandHandler("cancel", get_url.cancel)]
)
