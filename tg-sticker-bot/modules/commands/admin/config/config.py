import logging

from telegram import (
    InlineKeyboardButton
    , InlineKeyboardMarkup
)
from telegram.ext import ConversationHandler

from library.constants import ConversationState
from modules.db import DBManagers
import settings.settings as settings

logger = logging.getLogger(__name__)

settings_manager = settings.SettingsManager()
dbm = DBManagers()

# using __all__ to not expose _build_config_console
__all__ = [ 'start', 'select', 'edit', 'cancel', 'exit_config' ]


def _build_config_console(username):
	"""Build the text and button of config console"""

	# Get editable config from settings_manager
	edit_config_dict = settings_manager.get_editable_config()

	# Build reply_text & reply_keyboard
	reply_keyboard = []
	reply_text = "Welcome back admin <b>{}</b>!\n" \
				"Please select the config you want to change.\n\n" \
				"<b>Current config:</b>\n".format(username)
	for key, value in edit_config_dict.items():
		reply_text += "{}: {}\n".format(key, value)
		reply_keyboard.append([InlineKeyboardButton(
			"Edit {}".format(key), callback_data=key)])

	# Append Reload & Exit button to reply_keyboard
	reply_keyboard.append(
		[InlineKeyboardButton("Reload", callback_data="ReloadConfig"),
		InlineKeyboardButton("Exit", callback_data="ExitConfig")])

	return reply_text, reply_keyboard   # Return tuple


@dbm.logged(__name__)
def start(update, context):
	"""Start /config conversation
	Display config console
	"""
	chat_id = update.message.chat_id
	user = update.message.from_user

	# Check if user is admin
	if not settings_manager.is_admin(user.username):
		return ConversationHandler.END

	logger.info("[%s] Admin %s issued %s", chat_id,
				user.username, update.message.text)

	# Build config console
	reply_text, reply_keyboard = _build_config_console(user.username)

	# Output config console
	# Store console's message id into config_console_id
	context.user_data["config_console_id"] = update.message.reply_text(
		reply_text,
		reply_markup=InlineKeyboardMarkup(reply_keyboard),
		parse_mode="HTML").message_id

	return ConversationState.CONFIG_SELECT


@dbm.logged(__name__)
def select(update, context):
	"""Select config to mutate"""
	query = update.callback_query
	username = query.message.chat.username
	chat_id = query.message.chat_id

	if (query.data == "ExitConfig"):
		# Exit config console
		return exit_config(update, context)

	elif (query.data == "ReloadConfig"):
		# Reload config using settings_manager
		settings_manager.reload_config()

		logger.info("[%s] Admin %s reloaded config.", chat_id,
					username)

		# Replace old config console with text
		# Alert admin config has been reloaded
		context.bot.edit_message_text(
			text="Config reloaded successfully.",
			chat_id=chat_id,
			message_id=query.message.message_id)

		# Build new config console
		reply_text, reply_keyboard = _build_config_console(username)

		# Output new config console
		# Assign new console's message id to config_console_id
		context.user_data["config_console_id"] = context.bot.send_message(
			chat_id=chat_id,
			text=reply_text,
			reply_markup=InlineKeyboardMarkup(reply_keyboard),
			parse_mode="HTML").message_id

		return ConversationState.CONFIG_SELECT

	# Set the target config key
	context.user_data["edit_config_key"] = query.data

	context.bot.edit_message_text(
		text="Selected config: <b>{}</b>\n"
			"Current value: <b>{}</b>\n"
			"Type /cancel to cancel.\n\n"
			"Please enter new value.".format(
				query.data, settings_manager.get_config(query.data)),
		chat_id=chat_id,
		message_id=query.message.message_id,
		parse_mode="HTML")

	return ConversationState.CONFIG_EDIT


@dbm.logged(__name__)
def edit(update, context):
	"""Edit selected config with admin's input"""
	edit_config_key = context.user_data["edit_config_key"]
	chat_id = update.message.chat_id
	user = update.message.from_user

	# Get original value for output
	original_value = settings_manager.get_config(edit_config_key)
	# Edit config using settings_manager
	settings_manager.edit_config(edit_config_key, update.message.text)
	logger.info("[%s] Admin %s changed config '%s' from %s to %s", chat_id,
				user.username, edit_config_key, original_value, update.message.text)

	# Build new config console
	reply_text, reply_keyboard = _build_config_console(user.username)

	# Replace old config console
	# Alert admin change has been applyed
	context.bot.edit_message_text(
		text="Config '{}' has been changed from {} to {} successfully.".format(
			edit_config_key, original_value, update.message.text),
		chat_id=chat_id,
		message_id=context.user_data["config_console_id"],
		parse_mode="HTML")

	# Empty edit_config_key
	del context.user_data["edit_config_key"]

	# Output new config console
	# Assign new console's message id to config_console_id
	context.user_data["config_console_id"] = update.message.reply_text(
		reply_text,
		reply_markup=InlineKeyboardMarkup(reply_keyboard),
		parse_mode="HTML").message_id

	return ConversationState.CONFIG_SELECT


@dbm.logged(__name__)
def cancel(update, context):
	"""Cancel config edit, go back to config console"""
	user = update.message.from_user

	# Rmove edit_config_key
	del context.user_data["edit_config_key"]

	# Build new config console
	reply_text, reply_keyboard = _build_config_console(user.username)

	# Alert admin config edit has been cancelled
	update.message.reply_text(
		"Config edit cancelled.",
		parse_mode="HTML")

	# Output new config console
	# Assign new console's message id to config_console_id
	context.user_data["config_console_id"] = update.message.reply_text(
		reply_text,
		reply_markup=InlineKeyboardMarkup(reply_keyboard),
		parse_mode="HTML").message_id

	return ConversationState.CONFIG_SELECT


@dbm.logged(__name__)
def exit_config(update, context):
	"""Exit config console, Replace config console with text
	Also will be hit if any message is typed while config console is present
	"""
	# Remove config console from chat
	context.bot.edit_message_text(
		text="Exited config console.",
		chat_id=update.effective_chat.id,
		message_id=context.user_data["config_console_id"])

	# Remove config_console_id
	del context.user_data["config_console_id"]

	return ConversationHandler.END
