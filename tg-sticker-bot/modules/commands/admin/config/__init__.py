from telegram.ext import (
    CallbackQueryHandler, CommandHandler, Filters,
    ConversationHandler, MessageHandler
)
from library.constants import ConversationState
from . import config

# Declare conversation handler for /config
handler = ConversationHandler(
    entry_points=[
        CommandHandler("config", config.start)
    ],
    states={
        ConversationState.CONFIG_SELECT: [
            CallbackQueryHandler(config.select)
        ],
        ConversationState.CONFIG_EDIT: [
            CommandHandler("cancel", config.cancel),
            MessageHandler(Filters.text, config.edit)
        ]
    },
    fallbacks=[
        MessageHandler(Filters.text, config.exit_config)
    ]
)