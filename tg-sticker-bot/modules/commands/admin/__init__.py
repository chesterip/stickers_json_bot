import logging
import os
import zipfile
from datetime import datetime
from glob import glob
from io import BytesIO

import ujson as json
from telegram.ext.dispatcher import run_async

import settings.settings as settings
from modules.db import DBManagers
from modules.usage_stats import get_stickers_usage
from modules.utils.image_process import preview_generate
from . import broadcast, config
settings_manager = settings.SettingsManager()
dbm = DBManagers()

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)
logger = logging.getLogger(__name__)


@settings_manager.admin_only
@dbm.logged(__name__)
def cache(update, context):
    """Admin only commad, all related to cache
    /cache:       List out cached stickers (.json) in cache
    /cache fix:   Remove any cached stickers (.json) with no preview file (.webp)
    /cache purge: Remove old and unpopular cached stickers (.json) and corresponding preview file (.webp)"""
    chat_id = update.message.chat_id
    user = update.message.from_user

    output_text = ""

    # Using args instead of checking update.message.text
    if "fix" in context.args:
        # /cache fix:   delete any JSON file without preview
        for name in glob("cache/*.json"):
            if not os.path.isfile(name + ".webp"):
                logger.info("[%s] User %s /cache fix removed %s",
                            chat_id, user.username, name)
                os.remove(name)
                output_text += name + "\n"

        if output_text == "":   # No file has been removed
            context.bot.send_message(
                chat_id=chat_id, text="No fix is required")

        else:  # List out all deleted JSON file
            context.bot.send_message(chat_id=chat_id,
                                     text="<b>Following files have been removed:</b>\n\n" + output_text,
                                     parse_mode="HTML")
    elif "purge" in context.args:
        # stickers_usage = get_stickers_usage(force_update=True)
        purged_counter = 0
        for sticker_json in glob("cache/*.json"):
            # check if the set was created before purge_cache_time_crtical (in seconds)
            delta_time = datetime.now().timestamp() - os.path.getmtime(sticker_json)
            if delta_time > settings_manager.get_config("purge_cache_time_crtical"):

                # convert complete path to basename without extension
                key = os.path.splitext(os.path.basename(sticker_json))[0]

                # if the sticker set is one part only, remove the last underscore and number
                if key[-1].isdigit() and key[-2] == "_":
                    key = key[:-2]

                # check if the stickers set is unpopular
                if key in stickers_usage:
                    if stickers_usage[key] > settings_manager.get_config("purge_cache_usage_crtical"):
                        # the set is old, but popular
                        continue

                # the sticker set is either unpopular, or missing in stickers usage
                # it and its preview image would be removed

                os.remove(sticker_json)
                logger.info("cache purge: %s was removed", sticker_json)
                purged_counter += 1
                if os.path.exists(sticker_json + ".webp"):
                    os.remove(sticker_json + ".webp")
                    logger.info("cache purge: %s was removed",
                                sticker_json + ".webp")

        context.bot.send_message(
            chat_id=chat_id,
            text=f"<b>{purged_counter}</b> files have been removed.",
            parse_mode="HTML"
        )

        logger.info(
            "cache purge: %s files have been removed",
            purged_counter
        )
    else:
        # /cache:   List out JSON file(s) in cache
        context.bot.send_message(chat_id=chat_id, text="Gathering cache list")
        output_text += "<b>Cache List:\n\n</b>"
        output_text += "\n".join(
            [os.path.basename(name) for name in glob("cache/*.json")]
        )
        context.bot.send_message(
            chat_id=chat_id,
            text=output_text,
            parse_mode="HTML"
        )


@settings_manager.admin_only
@dbm.logged(__name__)
def log(update, context):
    """Admin only commad, output logs to admin
    /log:         Output log"""
    chat_id = update.message.chat_id
    user = update.message.from_user

    buffer = BytesIO()
    buffer.name = 'log.txt'

    # Prepare template for each line in log
    keys = ('date', 'module', 'username', 'text')
    line_template = '\t'.join(['{' + key + '}'for key in keys]) + '\n'

    # Get log data from db, and write to buffer
    logs = dbm.log.find()
    for entry in logs:
        [entry.setdefault(key)for key in keys]
        buffer.write(line_template.format(**entry).encode('utf-8'))

    buffer.seek(0)
    context.bot.send_document(chat_id=chat_id, document=buffer)


@settings_manager.admin_only
@dbm.logged(__name__)
@run_async
def build_preview(update, context):
    """Build preview for a sticker set in cache
    /buildprev $FILENAME - build preivew for a specific sticker set
    /buildprev all - rebuild preivew for all sticker sets in cache
    /buildprev fix - build preivew for sticker sets without a preview image in cache
    """
    if context.args:
        if "all" in context.args:
            counter = 0
            for sticker_set in glob("cache/*.json"):
                with open(sticker_set, encoding="utf-8") as data_file:
                    data = [sticker_set, json.load(data_file)]
                preview_generate(data, logger=logger)
                counter += 1

            update.message.reply_text(
                str(counter) + " previews have been generated")

        elif "fix" in context.args:
            counter = 0
            for sticker_set in glob("cache/*.json"):
                if not os.path.exists(sticker_set + ".webp"):
                    with open(sticker_set, encoding="utf-8") as data_file:
                        data = [sticker_set, json.load(data_file)]
                    preview_generate(data, logger=logger)
                    counter += 1
            update.message.reply_text(
                str(counter) + " previews have been generated")

        else:
            target = update.message.text.replace("/buildprev ", "cache/")
            with open(target, encoding="utf-8") as data_file:
                data = [target, json.load(data_file)]
            preview_generate(data, logger=logger)
    else:
        update.message.reply_text("No args you dumb bitch")
