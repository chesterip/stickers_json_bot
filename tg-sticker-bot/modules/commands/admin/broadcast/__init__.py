from telegram.ext import (
    CallbackQueryHandler, CommandHandler, Filters,
    ConversationHandler, MessageHandler
)
from library.constants import ConversationState
from . import broadcast

# Declare conversation handler for /broadcast
handler = ConversationHandler(
    entry_points=[CommandHandler("broadcast", broadcast.start)],
    states={
        ConversationState.BROADCAST: [
            CommandHandler("cancel", broadcast.cancel),
            MessageHandler(Filters.text, broadcast.send)
        ]
    },
    fallbacks=[CommandHandler("cancel", broadcast.cancel)]
)