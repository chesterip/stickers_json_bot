import logging

from telegram import ChatAction, TelegramError
from telegram.ext import ConversationHandler
from telegram.ext.dispatcher import run_async

from library.constants import ConversationState
from modules.db import DBManagers
import settings.settings as settings

logger = logging.getLogger(__name__)

settings_manager = settings.SettingsManager()
dbm = DBManagers()


@dbm.logged(__name__)
def start(update, context):
    """Start /broadcast conversation"""
    user = update.message.from_user
    # Check if user is admin
    if not settings_manager.is_admin(user.username):
        return ConversationHandler.END

    chat_id = update.message.chat_id
    logger.info("[%s] Admin %s issued %s", chat_id,
                user.username, update.message.text)

    # Display message to alert admin that bot have entered broadcast mode
    update.message.reply_text(
        f"Welcome back admin <b>{user.username}</b>!\n"
        "Please type in the message you want to broadcast.\n"
        "Type /cancel to cancel.",
        parse_mode="HTML")

    return ConversationState.BROADCAST


@dbm.logged(__name__)
@run_async
def send(update, context):
    """Broadcast inputted message"""
    chat_id = update.message.chat_id
    user = update.message.from_user
    logger.info("[%s] Admin %s broadcasted a message: %s", chat_id,
                user.username, update.message.text)

    # Send out broadcast message 1-by-1
    total_message = 0
    deleted_chats = []
    for chat_user in settings_manager.get_all_chats():
        try:
            # Send chat action first to check if bot is deleted from user
            context.bot.send_chat_action(chat_id=chat_user, action=ChatAction.TYPING)

            # Send broadcast message, will not be run if send_chat_action failed
            context.bot.send_message(
                chat_id=chat_user,
                text=update.message.text,
                disable_web_page_preview=True,
                parse_mode="HTML")

            total_message += 1
        except TelegramError:
            # Chat is deleted by user, mark chat_id for further deletion
            deleted_chats.append(chat_user)

    # Remove any chat_id from chats that user deleted
    if deleted_chats:
        for chat in deleted_chats:
            settings_manager.remove_chat(chat)

    # Display message to alert admin broadcast is finished and display total message sent
    context.bot.send_message(
        chat_id=chat_id,
        text="<b>Broadcast finished!</b>\nSent to <b>{}</b> user(s)".format(total_message),
        parse_mode="HTML")

    return ConversationHandler.END


@dbm.logged(__name__)
def cancel(update, context):
    """Cancel broadcast conversation"""
    update.message.reply_text("Broadcast cancelled.")

    return ConversationHandler.END