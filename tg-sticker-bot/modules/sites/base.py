import os
from abc import ABC, abstractmethod
from enum import Enum


class SiteDownloaderBase(ABC):
    def __init__(self, target):
        self.respond = None
        self.result = None
        self.status = None
        self.target = target
        os.makedirs(os.path.join('cache', self.module), exist_ok=True)

    @property
    def module(self):
        return self.__class__.__module__.split('.')[-1]

    @property
    def temp_location(self):
        os.makedirs(os.path.join('temp', self.module,
                                 self.target), exist_ok=True)
        return os.path.join('temp', self.module, self.target)

    @property
    def cache_location(self):
        return os.path.join('cache', self.module, self.target)

    def mkdir_temp(self):
        return os.makedirs(self.temp_location, exist_ok=True)

    @classmethod
    def get_url(cls, path):
        if hasattr(cls, 'base_url'):
            return cls.base_url + os.path.basename(path)
        return os.path.basename(path)

    @abstractmethod
    def download(self):
        """
        Download sticker set, and return status and stickers as a dict:
        Act as a controller, pass data to corresponding services
        {
        "metadata": { (metadata to be displayed in WhatsApp)
            "identifier": str,
            "name": str,
            "publisher": str
        },
        "stickers_list": [str (absolute path to each sticker)],
        "tray_path": str (absolute path to tray's icon),
        "filename": str
        }
        """
        self.mkdir_temp()
        self.result = {
            "success": True
        }

    @property
    def sticker_set(self):
        if self.result is None:
            self.download()
        return self.result
