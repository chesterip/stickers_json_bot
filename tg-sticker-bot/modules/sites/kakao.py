# Author: @chesterip
# Refactor by: @leotang1993

import os
import re

import requests
import ujson as json
from modules.utils.generic import http_downloader
from modules.sites.base import SiteDownloaderBase
from modules.db import DBManagers


PATTERN = re.compile(r'itemCode&quot;:&quot;(\d+)&quot;')
dbm = DBManagers()

@dbm.register_site(__name__)
class SiteDownloader(SiteDownloaderBase):
    base_url = "e.kakao.com/t/"

    def download(self):
        super().download()
        store_url = "https://e.kakao.com/t/" + self.target
        url_list = {}

        # Go to store page and read image set's inner id
        respond = http_downloader(url=store_url, save_to_file=False)

        # If download unsucessful, return E03
        if respond["status"] != 200:
            self.result["error"] = "E03-" + str(respond["status"])
            self.result["success"] = False
        else:
            match = PATTERN.findall(respond["text"])

            # Get all images' individual url, put them in url_list
            if match:
                # Separate variable for readability (store_url & inner_url)
                inner_url = "https://e.kakao.com/detail/thumb_url?item_code=" + \
                    match[0]

                # Download image set's metadata
                respond = requests.get(inner_url)
                url_list = respond.json()

            if url_list and url_list["status"] == 0:
                stickers_list = []

                # Create folder for storing image set
                self.mkdir_temp()

                # Download images 1-by-1
                for sticker in url_list["body"]:
                    filename = os.path.join(self.temp_location, os.path.basename(sticker) + ".png")
                    http_downloader(url=sticker, path=filename)
                    stickers_list.append(filename)

                self.result["stickers_set"] = {
                    "metadata": {
                        "identifier": self.target,
                        "name": self.target,
                        "publisher": self.target
                    },
                    "stickers_list": stickers_list,
                    "tray_path": stickers_list[0],
                    "filename": self.cache_location
                }
            else:
                self.result["error"] = "E05"
                self.result["success"] = False

        return self.result
