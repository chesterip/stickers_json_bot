# Author: @chesterip
# Refactor by: @leotang1993

import os
import re
import zipfile

from modules.utils.generic import http_downloader
from modules.sites.base import SiteDownloaderBase

import ujson as json

NAME_PATTERN = re.compile(r'.*mdCMN08Ttl">(.*)<\/h3>')
AUTHOR_PATTERN = re.compile(r'emojishop\/author\/\d+\/zh-Hant">(.*)<\/a>')


class SiteDownloader(SiteDownloaderBase):
    # Extract image from line emoji store
    def download(self):
        super().download()

        # Download image set zip
        respond = http_downloader(
            url=f"https://sdl-stickershop.line.naver.jp/sticonshop/v1/{self.target}/sticon/iphone/package.zip",
            path=self.temp_location + ".zip"
        )

        # If download unsucessful, return E03
        if respond["status"] != 200:
            self.result["error"] = "E03-" + str(respond["status"])
            self.result["success"] = False
        else:
            # Unzip image set, then delete the zip file
            with zipfile.ZipFile(self.temp_location + ".zip", "r") as zip_ref:
                zip_ref.extractall(self.temp_location)
            os.remove(self.temp_location + ".zip")

            # Read metadata
            with open(self.temp_location + '/meta.json', encoding="utf-8") as data_file:
                data =json.load(data_file)

            metadata = {
                "identifier": self.target,
                "name": self.target,
                "publisher": self.target
            }

            stickers_list = [
                "{}/{}.png".format(self.temp_location, sticker)
                for sticker in data["orders"]
            ]

            tray_path = stickers_list[0]

            # Collect additional metadata from store page
            url = f"https://store.line.me/emojishop/product/{self.target}/zh-Hant"
            respond = http_downloader(url=url, save_to_file=False)
            contents = respond["text"]

            match = NAME_PATTERN.findall(contents)
            if match:
                metadata["name"] = match[0]

            match = AUTHOR_PATTERN.findall(contents)
            if match:
                metadata["publisher"] = match[0]

            self.result["stickers_set"] = {
                "metadata": metadata,
                "stickers_list": stickers_list,
                "tray_path": tray_path,
                "filename": self.cache_location
            }

        return self.result
