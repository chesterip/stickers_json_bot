# Author: @chesterip
# Refactor by: @leotang1993

import shutil

import requests

import ujson as json
from modules.sites.base import SiteDownloaderBase
from modules.db import DBManagers
dbm = DBManagers()

@dbm.register_site(__name__)
class SiteDownloader(SiteDownloaderBase):
    base_url = "whatsticker.online/p/"

    # For whatsticker, just download and return path to the json file
    def download(self):
        respond = requests.get("https://ios.whatsticker.online/cache/pack.php"
                            "?id={}".format(self.target),
                            stream=True,
                            headers={'User-agent': 'Mozilla/5.0'})

        if respond.status_code == 200:
            # Write JSON responds into file
            with open(self.cache_location + '.json', 'wb') as json_file:
                respond.raw.decode_content = True
                shutil.copyfileobj(respond.raw, json_file)

            # Read just created JSON file
            with open(self.cache_location + '.json', encoding="utf-8") as data_file:
                data =json.load(data_file)

            return [(self.cache_location + '.json', data)]
        return []
