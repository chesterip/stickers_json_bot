# Author: @chesterip
# Refactor by: @leotang1993

import os

import requests
import ujson as json
from tgs import parsers
from tgs.exporters import exporters

from modules.sites.base import SiteDownloaderBase
from modules.db import DBManagers
import settings.settings as settings

GET_SET_ADDR = "https://api.telegram.org/bot{}/GetStickerSet?name={}"


dbm = DBManagers()

@dbm.register_site(__name__)
class SiteDownloader(SiteDownloaderBase):
    base_url = "t.me/addstickers/"

    @staticmethod
    def tgs_to_png(path):
        """Convert Telegram animated sticker to png"""
        exporters.get("png").export(
            parsers.tgs.parse_tgs(path),
            path+".png", {}
        )

    def download(self, bot, telegram_data=""):
        """Extract image from telegram server"""
        super().download()


        # Sticker set has not been cached
        if telegram_data == "":
            settings_manager = settings.SettingsManager()

            # Download image set's metadata
            respond = requests.get(GET_SET_ADDR.format(
                settings_manager.get_config("token"),
                self.target
            ))
            data = respond.json()

        else:
            # Sticker set is previously cached but different from telegram server
            data = telegram_data

        # Read metadata
        if data["ok"]:
            stickers_list = []
            metadata = {
                "identifier": data["result"]["name"],
                "name": data["result"]["title"],
                "publisher": self.target
            }


            # Download image 1-by-1
            for sticker in data["result"]["stickers"]:
                if sticker["is_animated"]:
                    image_file = bot.get_file(sticker["file_id"]).download(
                        custom_path=f'{self.temp_location}/{sticker["file_id"]}.tgs'
                    )
                    SiteDownloader.tgs_to_png(image_file)
                    stickers_list.append(
                        f'{self.temp_location}/{sticker["file_id"]}.tgs.png')

                else:
                    image_file = bot.get_file(sticker["file_id"]).download(
                        custom_path=f'{self.temp_location}/{sticker["file_id"]}.webp'
                    )
                    stickers_list.append(image_file)

            tray_path = stickers_list[0]

            self.result["stickers_set"] = {
                "metadata": metadata,
                "stickers_list": stickers_list,
                "tray_path": tray_path,
                "filename": self.cache_location
            }
        else:
            self.result["error"] = "E04"
            self.result["success"] = False
        return self.result
