# Author: @chesterip
# Refactor by: @leotang1993

import os
import re
import zipfile

import ujson as json
from modules.utils.generic import http_downloader
from modules.sites.base import SiteDownloaderBase
from modules.db import DBManagers


dbm = DBManagers()


@dbm.register_site(__name__)
class SiteDownloader(SiteDownloaderBase):
    base_url = "line.me/S/sticker/"

    # Extract image from line sticker store
    def download(self):
        super().download()

        url = "http://dl.stickershop.line.naver.jp/products/0/0/1/" + \
            self.target + "/iphone/stickers@2x.zip"

        # Download image set zip
        respond = http_downloader(url=url, path=self.temp_location + ".zip")

        if respond["status"] != 200:
            # If download is unsuccessful, return E03
            self.result["error"] = "E03-" + str(respond["status"])
            self.result["success"] = False

        else:
            # Unzip image set, then delete the zip file
            with zipfile.ZipFile(self.temp_location + ".zip", "r") as zip_ref:
                zip_ref.extractall(self.temp_location)
            os.remove(self.temp_location + ".zip")

            # Read metadata
            with open(self.temp_location + '/productInfo.meta', encoding="utf-8") as data_file:
                data = json.load(data_file)

            metadata = {
                "identifier": self.target + '_' + re.sub(r"[^A-Za-z]+", '', data["title"]["en"]),
                "name": data["title"]["zh-Hant"] if "zh-Hant" in data["title"] else data["title"]["en"],
                "publisher": data["author"]["zh-Hant"] if "zh-Hant" in data["author"] else data["author"]["en"]
            }

            stickers_list = [
                "{}/{}@2x.png".format(self.temp_location, sticker['id'])
                for sticker in data["stickers"]
            ]

            tray_path = self.temp_location + "/tab_on@2x.png"

            self.result["stickers_set"] = {
                "metadata": metadata,
                "stickers_list": stickers_list,
                "tray_path": tray_path,
                "filename": self.cache_location
            }

        return self.result
