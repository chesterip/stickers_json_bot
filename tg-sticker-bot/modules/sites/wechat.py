# Author: @chesterip
# Refactor by: @leotang1993

import os
import re
from hashlib import md5

import ujson as json
from modules.utils.generic import http_downloader
from modules.sites.base import SiteDownloaderBase


class SiteDownloader(SiteDownloaderBase):
# Extract image from wechat
    def download(self):
        super().download()

        name = md5(self.target.encode()).hexdigest()
        stickers_list = []

        # Go to store page for image's individual url, put them in url_list
        respond = http_downloader(
            url=self.target,
            save_to_file=False)

        # If download unsucessful, return E03
        if respond["status"] != 200:
            self.result["error"] = "E03-" + str(respond["status"])
            self.result["success"] = False

        else:
            url_list = re.findall(
                r'<img class="stiker_content_ele" src="(http:\/\/mmbiz.qpic.cn\/mmemoticon\/.*\/0)" \/>', respond["text"])

            # Create folder for storing image set
            os.makedirs(os.path.join('temp', self.module, name), exist_ok=True)

            # Download image 1-by-1
            for ii, sticker_url in enumerate(url_list):
                filename = os.path.join('temp', self.module, name, str(ii) + ".png")
                http_downloader(url=sticker_url, path=filename)
                stickers_list.append(filename)

            tray_path = stickers_list[0]

            self.result["stickers_set"] = {
                "metadata": {
                    "identifier": name,
                    "name": re.findall(r'<h2 class="stiker_head_msg_title">(.*)<\/h2>', respond["text"])[0],
                    "publisher": re.findall(r'<p class="stiker_head_ft">(.*)<\/p>', respond["text"])[0]
                },
                "stickers_list": stickers_list,
                "tray_path": tray_path,
                "filename": os.path.join('cache', self.module, name)
            }

        return self.result
