import os
import re
import time
import zipfile
from datetime import datetime
from glob import glob
from modules.db import DBManagers

import ujson as json


dbm = DBManagers()


def archive_old(t_crit=(72*60*60)):
    """Archive json files if its last modified date is before t_crit (in seconds),
    default t_crit is 3 days
    """
    time_now = time.time()
    for sticker in dbm.stickers.find({
        "archive": {"$exists": False},
        "request_count": {"$lt": 20},
        "last_request": {"$lt": time_now - t_crit}
    }):
        # TODO resolve the path from data
        path_to_sticker_set = ""

        # Update sticker with flag, archive
        # dbm.stickers.update_one(
        #     {
        #         '_id': sticker['_id'],
        #     },
        #     {
        #         "$set": {"archive": True}
        #     }
        # )

        # with zipfile.ZipFile(f'{sticker_set}.zip', 'w', compression=zipfile.ZIP_DEFLATED) as zip:
        #     zip.write(sticker_set)
        # os.remove(sticker_set)

        # if os.path.exists(f"{sticker_set}.webp"):
        #     os.remove(f"{sticker_set}.webp")


def get_stickers_usage(force_update=False):
    """Get the sticker usages, as well as archive old stickers"""
    pass
