# Author: @leotang1993

import logging
from threading import Timer

import settings.settings as settings

# Enable logging
logger = logging.getLogger("store_chats_scheduler")

settings_manager = settings.SettingsManager()


# Store global chats into JSON file
def store_chats(force_save=False):
    settings_manager.write_chats(force_save)
    logger.info("chats stored to chats.json")


# Using recursion to schedule store_chats according to config set interval
def store_chats_scheduler(init=False):
    store_chats_intervel = settings_manager.get_config("store_chats_intervel")

    store_chats_thread = Timer(
        store_chats_intervel, store_chats_scheduler)
    store_chats_thread.daemon = True    # Make sure thread is killed when updater stop
    if not init:    # Don't store chats when bot initializing
        store_chats()
    store_chats_thread.start()
