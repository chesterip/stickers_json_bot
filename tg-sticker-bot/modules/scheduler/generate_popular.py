# Author: @leotang1993

import logging
from threading import Timer

import settings.settings as settings
from modules.popular import generate_popular_preview

# Enable logging
logger = logging.getLogger("generate_popular_scheduler")

settings_manager = settings.SettingsManager()


# generate popular preview using popular module
def generate_popular():
    generate_popular_preview()
    logger.info("New popular preview generated.")


# Using recursion to schedule _store_chats according to config set interval
def generate_popular_scheduler(init=False):
    generate_popular_intervel = settings_manager.get_config("generate_popular_intervel")

    generate_popular_thread = Timer(
        generate_popular_intervel, generate_popular_scheduler)
    generate_popular_thread.daemon = True    # Make sure thread is killed when updater stop
    if not init:    # Don't generate preview when bot initializing
        generate_popular()
    generate_popular_thread.start()
