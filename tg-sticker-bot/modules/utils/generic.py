# Author: @chesterip
# Refactor by: @leotang1993

import logging
import math
import time
import os

import requests
import ujson as json
from modules.utils.image_process import image_to_sticker, resize
from PIL import Image


logger = logging.getLogger(__name__)


def http_downloader(url, path="", save_to_file=True):
    respond = requests.get(
        url,
        stream=True,
        headers={'User-agent': 'Mozilla/5.0'}
    )

    result = {
        "status": respond.status_code
    }

    # save to file, or decode + return the text
    if respond.status_code == 200:
        if save_to_file:
            open(path, "wb").write(respond.content)
        else:
            result["text"] = respond.text
    return result


def generate_json(obj):
    """Read data from sticker object"""
    metadata = obj["metadata"]
    stickers_list = obj["stickers_list"]
    tray_path = obj["tray_path"]
    dest_name = obj["filename"]

    start = time.time()

    # Resize image and encode in base64
    conv_start = time.time()
    stickers =[image_to_sticker(image)for image in stickers_list]
    metadata["tray_image"] = resize(Image.open(tray_path), 96, "PNG")
    conv_time = time.time() - conv_start

    logger.info(
        "{} stickers and tray image have been converted in {:.3f} s".format(
            len(stickers_list), conv_time
        )
    )

    # Define JSON file generation variable
    small_set = len(stickers) <= 30
    ii = 0
    original_iden = metadata["identifier"]
    json_arr = []

    # Calculate batch size
    # Default batch size = 30
    # Each batch cannot have more than 30 stickers
    batch_size = 30
    if not small_set:
        total_batch_number = math.ceil(len(stickers) / 30)
        batch_size = len(stickers) // total_batch_number

    # Process stickers to JSON file by batch
    while stickers:
        ii += 1
        # Determine metadata of JSON file
        destination = "{}_{}.json".format(dest_name, ii)

        metadata["identifier"] = original_iden if small_set else f"{original_iden}_part{ii}"
        # Be consistent with string quotes, make up your damn mind at the start, use double quotes as stardard from now

        metadata["stickers"] = stickers[:batch_size]

        # Wrtie to cache folder
        with open(destination, "w", encoding="utf-8") as outfile:
            json.dump(metadata, outfile, ensure_ascii=False)

        logger.info(
            "FILE {} ({:7.2f} KiB) has been generated".format(
                destination, os.stat(destination).st_size / 1024
            )
        )

        # Remove processed batch
        del stickers[:batch_size]
        json_arr.append((destination, metadata.copy()))

    total_time = time.time() - start
    logger.info(f"{ii} json(s) have been generated in {total_time:.3f} s")

    return json_arr
