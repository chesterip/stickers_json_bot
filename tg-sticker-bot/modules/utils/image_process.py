import os
from base64 import b64decode, b64encode
from io import BytesIO

import ujson as json
from PIL import Image, ImageDraw, ImageFont
from tgs.exporters import exporters

import settings.settings as settings
from modules.db import DBManagers

dbm = DBManagers()

# resize and return result in base64


def resize(img, boxSize, img_format):
    if not (boxSize in img.size):
        # only when both dimensions of image are different from the boxSize, resize is required.
        if img.size[0] < img.size[1]:
            newSize = (int(img.size[0] / img.size[1] * boxSize), boxSize)
        else:
            newSize = (boxSize, int(img.size[1] / img.size[0] * boxSize))
        img = img.resize(newSize, Image.ANTIALIAS)

    # create an empty image with a size of boxSize * boxSize
    new_im = Image.new('RGBA', (boxSize, boxSize), (255, 0, 0, 0))
    new_im.paste(
        img, ((boxSize - img.size[0]) // 2, (boxSize - img.size[1]) // 2))

    # encode and store the image in buffer
    # for webp and png, some extra parameters are specified
    buffer = BytesIO()
    if img_format == "webp":
        new_im.save(buffer, format="webp", lossless=True, quality=100)
    elif img_format == "png":
        new_im.save(buffer, format="png", optimize=True)
    else:
        new_im.save(buffer, format=img_format)
    return str(b64encode(buffer.getvalue()), "utf-8")


# customized text draw function, ability to adjust font-spacing
def text_draw(draw, coordinate, font, fill, text, ratio):
    x, y = coordinate
    for character in text:
        draw.text((x, y), character, font=font, fill=fill)
        x += font.getsize(character)[0] * ratio


# generate a preview image in webp
def preview_generate(filename_metadata, logger=None, custompack=False):
    settings_manager = settings.SettingsManager()
    fonts = settings_manager.get_config("font_types")

    stickers = []
    if custompack:
        filename, metadata = filename_metadata
    else:
        (filename, metadata), source = filename_metadata
        source = "CUSTOMPACK"

    # read stickers from json file, decode and convert to Image object
    for sticker in metadata["stickers"]:
        img = Image.open(BytesIO(b64decode(
            sticker["image_data"])), mode="r")
        stickers.append(img)

    # load the template for preview image
    canvas = Image.open("images/new_prev.png").convert("RGBA")

    # draw stickers on preview image
    for jj, sticker_img in enumerate(stickers):
        sticker_img = sticker_img.resize(
            (118, 118), Image.ANTIALIAS
        ).convert("RGBA")
        canvas.paste(
            sticker_img,
            (12 + 124 * (jj % 5), 318 + (jj // 5) * 124),
            sticker_img
        )

    # draw the tray image
    img = Image.open(
        BytesIO(b64decode(metadata["tray_image"])),
        mode="r"
    ).convert("RGBA")
    img = img.resize((90, 90), Image.ANTIALIAS)
    canvas.paste(img, (39, 87), img)

    d = ImageDraw.Draw(canvas)

    # draw filename (XXXX.json) on the preview image
    fnt = ImageFont.truetype(fonts[0], 18)
    text_draw(draw=d, fill=(255, 255, 255, 255), font=fnt,
              coordinate=(30, 32), ratio=1.1,
              text=os.path.basename(filename))

    # draw the name of sticker set
    fnt = ImageFont.truetype(fonts[2], 32)
    d.text((154, 102), metadata["name"], font=fnt, fill=(255, 255, 255, 255))

    # draw the publisher of sticker set
    fnt = ImageFont.truetype(fonts[0], 18)
    text_draw(draw=d, fill=(255, 255, 255, 255), font=fnt,
              coordinate=(31, 196), text="created by ",
              ratio=1.2)

    fnt = ImageFont.truetype(fonts[1], 18)
    text_draw(draw=d, fill=(255, 255, 255, 255), font=fnt,
              coordinate=(140, 196), text=metadata["publisher"],
              ratio=1.2)

    url = (
        dbm.sites[source].get_url(filename.replace("_1.json", "")) + "\n"
    ) if source in dbm.sites else ""

    if url:
        fnt = ImageFont.truetype(fonts[0], 18)
        text_draw(draw=d, fill=(255, 255, 255, 255), font=fnt,
                  text="originated from",
                  coordinate=(31, 230), ratio=1.2)
        fnt = ImageFont.truetype(fonts[1], 18)
        text_draw(draw=d, fill=(255, 255, 255, 255), font=fnt,
                  text=url, coordinate=(31, 250), ratio=1.15)

    # save the preview as webp in RGB mode
    canvas = canvas.convert("RGB")
    canvas.save(
        filename + ".webp",
        format="webp", lossless=True, quality=100
    )
    if logger is not None:
        logger.info(f"Preview file {filename}.webp generated")
    else:
        print(f"Preview file {filename}.webp generated")


# generate a preview image for multiple sticker sets
def popular_preview_generate(sticker_sets_arr, logger=None):
    settings_manager = settings.SettingsManager()
    fonts = settings_manager.get_config("font_types")

    # load the template for preview image
    canvas = Image.open("images/popular.png").convert("RGBA")

    for progress, (filename, sticker_set, source) in enumerate(sticker_sets_arr):

        # read stickers from json file, decode and convert to Image object
        stickers = []
        for sticker in sticker_set["stickers"][:5]:
            img = Image.open(BytesIO(b64decode(
                sticker["image_data"])), mode="r")
            stickers.append(img)

        # draw stickers on preview image
        for jj, sticker_img in enumerate(stickers):
            sticker_img = sticker_img.resize(
                (118, 118), Image.ANTIALIAS).convert("RGBA")
            canvas.paste(
                sticker_img,
                (48 + 126 * jj, 310 + progress * 243),
                sticker_img
            )

        d = ImageDraw.Draw(canvas)

        # draw the name of sticker set
        fnt = ImageFont.truetype(fonts[2], 32)
        d.text(
            (45, 220 + 243 * progress),
            sticker_set["name"],
            font=fnt, fill=(255, 255, 255, 255)
        )

        if source in dbm.sites:
            url = dbm.sites[source].get_url(os.path.basename(
                filename).replace("_1.json", "")) + "\n"
            fnt = ImageFont.truetype(fonts[0], 18)

            text_draw(
                draw=d, fill=(255, 255, 255, 255), font=fnt,
                text=url, coordinate=(45, 260 + 243 * progress), ratio=1.15
            )

    # save the preview as webp in RGB mode
    canvas = canvas.convert("RGB")
    canvas.save(
        "cache/popular/popular.webp",
        format="webp", lossless=True, quality=100
    )


def image_to_sticker(path):
    """convert image to sticker format (webp in base64)"""
    return {"image_data": resize(Image.open(path), 512, "WEBP")}
