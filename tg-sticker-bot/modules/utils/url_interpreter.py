# Author: @chesterip
# Refactor by: @leotang1993
import os
import requests
from urllib.parse import parse_qs, urlparse
from hashlib import md5

from modules import sites

# Factor out the process of determining the target_id as sub-method for readability and maintainability
# Using urlparse instead of regex for better readability, need performance benchmark vs regex
# If its not much of a performance difference, readability is much more important
def extract_url_info(url):
    # Default, Nothing match
    t_site = None
    t_id = 0

    # Avoid urlphase error if scheme is not included in url
    if not (url.startswith("//") or url.startswith("http://") or url.startswith("https://")):
        url = "https://" + url

    # Parse the url
    parse_result = urlparse(url)
    hostname = parse_result.hostname

    # Remove prefix "www." from hostname if exists
    if hostname.startswith("www."):
        hostname = hostname[4:]

    # Fucking python have no case/switch statement wtf
    if hostname == "yabeline.tw":
        t_id = parse_qs(parse_result.query)["Number"][0]
        t_site = sites.line_sticker
    elif hostname == "store.line.me":
        if "emoji" in parse_result.path:
            t_site = sites.line_emoji
        else:
            t_site = sites.line_sticker
        t_id = parse_result.path.split("/")[3]
    elif hostname == "line.me":
        if "emoji" in parse_result.path:
            t_site = sites.line_emoji
            t_id = parse_qs(parse_result.query)["id"][0]
        else:
            t_site = sites.line_sticker
            t_id = parse_result.path.split("/")[3]
    elif hostname == "t.me":
        telegram_sticker_id = parse_result.path.split("/")[2]
        temp_t_id = telegram_sticker_id.replace(
            "line", "").replace("_by_Sean_Bot", "")
        if temp_t_id.isdigit():
            t_site = sites.line_sticker
            t_id = temp_t_id
        else:
            t_site = sites.telegram
            # Add prefix for cache identifier
            t_id = telegram_sticker_id
    elif hostname == "whatsticker.online":
        t_site = sites.whatsticker
        # Add prefix for cache identifier
        t_id = parse_result.path.split("/")[2]
    elif hostname == "e.kakao.com":
        t_site = sites.kakao
        # Add prefix for cache identifier
        t_id = parse_result.path.split("/")[2]
    elif hostname == "emoticon.kakao.com":
        # alternative URL to kakao emote icons
        # e.g https://emoticon.kakao.com/items/vMsN_F2eftEo7iMUG80VQvzW5t4=?lang=en&referer=share_lin
        respond = requests.get(url)
        if respond.status_code == 200:
            for line in respond.text.split("\n"):
                if "https://e.kakao.com/t/" in line:
                    t_site = sites.kakao
                    t_id = line.split("https://e.kakao.com/t/")[1][:-2]
                    break
    elif hostname == "sticker.weixin.qq.com":
        t_site = sites.wechat
        # Hard set scheme to https, avoid conflict with existing cache
        t_id = parse_result._replace(scheme="https").geturl()

    if t_site:
        site_name = t_site.__name__.split('.')[-1]
        if hostname == "sticker.weixin.qq.com":
            filename = os.path.join(site_name, md5(t_id.encode()).hexdigest())
        else:
            filename = os.path.join(site_name, t_id)
    else:
        filename = ""

    return t_site, t_id, filename    # Return tuple
