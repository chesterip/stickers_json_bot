#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Version 0.21
# Author: @chesterip
# Refactor by: @leotang1993

import logging
import os

import re
import shutil
import sys
import time
import zipfile
from io import BytesIO
from datetime import datetime
from glob import glob
from hashlib import md5
from threading import Thread

import requests
import settings.settings as settings
import ujson as json
from library.constants import ConversationState, Text
from modules import sites
from modules import commands
from modules.db import DBManagers
from modules.popular import popular_stickers
from modules.scheduler.generate_popular import generate_popular_scheduler
from modules.scheduler.store_chats import store_chats, store_chats_scheduler
from modules.usage_stats import get_stickers_usage
from modules.utils import generate_json, http_downloader
from modules.utils.image_process import preview_generate
from modules.utils.url_interpreter import extract_url_info
from telegram.ext import (
    CallbackQueryHandler, CommandHandler, Filters,
    ConversationHandler, MessageHandler, Updater
)
from telegram.ext.dispatcher import run_async

settings_manager = settings.SettingsManager()
dbm = DBManagers()

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)
logger = logging.getLogger(__name__)


@dbm.logged(__name__)
def help_text(update, context):
    """Display help information to user"""
    chat_id = update.message.chat_id
    user = update.message.from_user

    dbm.touch_user(chat_id)

    # Display help message
    context.bot.send_message(chat_id=chat_id, text=Text.HELP_MESSAGE,
                             disable_web_page_preview=True, parse_mode="MARKDOWN")

    # If user is admin, send help message for admin-only-commands
    if settings_manager.is_admin(user.username):
        context.bot.send_message(
            chat_id=chat_id, text=Text.ADMIN_HELP_MESSAGE, parse_mode="MARKDOWN")


@run_async
@dbm.logged(__name__)
def start_process(update, context):
    """Parse user request, extract images, and return JSON file for whatsapp sticker"""

    def get_cache(arr, filename):
        arr.extend(glob('cache/' + filename + '_*.json'))

    chat_id = update.message.chat_id
    user = update.message.from_user
    user_request = update.message.text

    # Cooldown mechanic, non-admin users have to wait a set amount of time
    if not settings_manager.is_admin(user.username):
        cooldown = settings_manager.get_config("limit")
        delta_t = int(round(datetime.now().timestamp())) - \
            settings_manager.get_chat_last_used(chat_id)

        if delta_t < cooldown:
            update.message.reply_text(
                'Please wait {} seconds before executing next command'.format(
                    cooldown - delta_t
                )
            )
            return False    # Exit point

    settings_manager.set_chat_last_used(
        chat_id, int(round(datetime.now().timestamp())))

    logger.info(
        "[%s] User %s requested %s",
        chat_id, user.username, user_request
    )

    extra_kwargs = {}

    if user_request.isdigit():
        # User-request's not digit, assume it's an url, extract its info
        target_site, target_id = sites.line_sticker, user_request
        filename = os.path.join('line_sticker', target_id)

    else:
        # Assign returned tuple
        target_site, target_id, filename = extract_url_info(user_request)

    # Terminate process if not support
    if target_site is None:
        update.message.reply_text("I don't understand, what's that?\n#E01")
        return False    # Exit point

    update.message.reply_text(
        f"Processing: `{target_id}`",
        disable_web_page_preview=True,
        parse_mode="MARKDOWN"
    )

    json_arr = []

    # See if the stickers exist in cache
    get_cache(json_arr, filename)

    # See if the stickers archive exists
    if not json_arr:
        zip_arr = glob('cache/' + filename + '_*.zip')

        if zip_arr:
            for zip_file in zip_arr:
                with zipfile.ZipFile(zip_file, 'r') as zip_ref:
                    zip_ref.extractall('.')
                os.remove(zip_file)
            get_cache(json_arr, filename)

    # For Sticker from Telegram, the bot instance needs to be passed.
    if target_site == sites.telegram:
        extra_kwargs['bot'] = context.bot

        # Check cached telegram set's size if target_site is telegram and cache exists
        if json_arr:
            # Calculate cached size
            cached_size = 0
            for cached_json in json_arr:
                with open(cached_json, "r") as json_file:
                    json_content = json.load(json_file)
                    cached_size += len(json_content["stickers"])

            # Check for sticker set's size on telegram server
            get_set_addr = "https://api.telegram.org/bot{}/GetStickerSet?name={}"
            respond = requests.get(
                get_set_addr.format(
                    settings_manager.get_config("token"), target_id
                )
            )
            extra_kwargs['telegram_data'] = respond.json()

            if extra_kwargs['telegram_data']["ok"] and len(extra_kwargs['telegram_data']["result"]["stickers"]) != cached_size:
                # Get sticker set from telegram server instead of returning cached version
                json_arr.clear()

    # No cache found, start the generation process
    if not json_arr:
        update.message.reply_text('Initialize collection of stickers')
        temp_json_arr = []

        site_dl = target_site.SiteDownloader(target_id)

        # When whatsticker is requested, just get the JSON from them
        if target_site == sites.whatsticker:

            temp_json_arr.extend(site_dl.download(**extra_kwargs))

        else:
            # Download the stickers
            stickers_downloader_result = site_dl.download(**extra_kwargs)

            # When download unccessful, log the error and abort the proceses
            if not stickers_downloader_result["success"]:
                logger.error(stickers_downloader_result["error"])
                update.message.reply_text(
                    "**Error occured.**\n\n#{error}".format(
                        **stickers_downloader_result
                    ),
                    parse_mode="HTML"
                )
                return False  # Exit point

            obj = stickers_downloader_result["stickers_set"]
            update.message.reply_text(
                '{} stickers have been collected'.format(len(obj["stickers_list"])))
            logger.info("{} stickers have been found".format(
                len(obj["stickers_list"]))
            )

            # Abort process if sticker set is too small for WhatsApp to work
            if len(obj["stickers_list"]) < 3:
                update.message.reply_text(
                    Text.NOT_ENOUGH_STICKERS, parse_mode="MARKDOWN")

                # Remove image folder before aborting
                shutil.rmtree(site_dl.temp_location)

                # Exit point
                return False

            # Generate JSON file
            update.message.reply_text('Generating JSON')
            temp_json_arr.extend(generate_json(obj))

            # Remove image folder
            shutil.rmtree(site_dl.temp_location)

        # Generate preview image
        update.message.reply_text('Generating preview')
        for image_json in temp_json_arr:
            preview_generate((image_json, target_site), logger=logger)

        get_cache(json_arr, filename)

    # JSON file ready
    if json_arr:
        update.message.reply_text(
            "Completed processing: `{}`\n`{}` json(s) have been generated".format(
                target_id, len(json_arr)
            ),
            disable_web_page_preview=True,
            parse_mode="MARKDOWN")

        for json_file in json_arr:
            update.message.reply_text('Now sending {} ({:7.2f} KiB)'.format(
                os.path.basename(json_file), os.stat(json_file).st_size / 1024))

            # Sending Preview
            if os.path.exists(json_file + '.webp'):
                context.bot.send_photo(
                    chat_id=chat_id,
                    photo=open(json_file + '.webp', 'rb')
                )

            # Sending JSON file
            context.bot.send_document(
                chat_id=chat_id, document=open(json_file, 'rb')
            )

        dbm.touch_sticker(
            target=target_id,
            source=target_site.__name__
        )

        logger.info(
            "[%s] User %s received %s",
            chat_id, user.username, target_id
        )

    else:
        update.message.reply_text(
            "Oh, something went wrong, there's nothing I can send you.\n#E02"
        )


def main():
    """Start the bot."""

    # Create cache folder if not exists
    if not os.path.exists("cache"):
        os.makedirs("cache")

    if not os.path.exists("cache/popular"):
        os.makedirs("cache/popular")

    # Create the EventHandler and pass it your bot's token.
    updater = Updater(
        settings_manager.get_config("token"),
        request_kwargs={'read_timeout': 6, 'connect_timeout': 7},
        workers=settings_manager.get_config("updater_workers"),
        use_context=True
    )

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # Call schedulers
    store_chats_scheduler(init=True)
    generate_popular_scheduler(init=True)

    # Restart the bot at server level
    def _stop_and_restart():
        """Gracefully stop the Updater and replace the current process with a new one"""
        updater.stop()
        os.execl(sys.executable, sys.executable, *sys.argv)

    # Handler method for restarting bot
    def _restart_handler(update, context):
        update.message.reply_text("Bot is restarting...")
        store_chats(force_save=True)
        Thread(target=_stop_and_restart).start()

    # Hook command handlers to dispatcher
    dp.add_handler(CommandHandler("r", _restart_handler))
    dp.add_handler(CommandHandler("cache", commands.admin.cache))
    dp.add_handler(CommandHandler("log", commands.admin.log))
    dp.add_handler(CommandHandler("buildprev", commands.admin.build_preview))

    dp.add_handler(CommandHandler(("start", "help"), help_text))
    dp.add_handler(CommandHandler("popular", popular_stickers))

    # Hook conversation handlers to dispatcher
    dp.add_handler(commands.admin.broadcast.handler)
    dp.add_handler(commands.admin.config.handler)
    dp.add_handler(commands.custom_pack.handler)
    dp.add_handler(commands.get_url.handler)

    # Hook message handler to dispatcher i.e sticker links or line sticker id
    # This need to be hooked after conversation handler,
    # else this will override any MessageHandler in ConversationHandler
    dp.add_handler(MessageHandler(Filters.text, start_process))

    # Log all errors
    dp.add_error_handler(dbm.error_log)

    # Start the Bot
    updater.start_polling()
    logger.info("Bot initiated (version: 0.21)")

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
