# Telegram WhatsApp-Sticker Conversion Bot

This bot is built to convert various formats of sticker to WhatsApp-format, and is meant to be served through Telegram.

## Commands Available

+ `help` - read the help message
+ `custompack` - create your own sticker pack
+ `geturl` - get the URL for a Telegram sticker pack
+ `popular` - get the top ten most requested stickers sets

## Compression Algorithm Comparison

The cache can grow really fast. Compression is a good tool to control the growth other than purging the cache.
Several algorithms are available using the `zipfile` module, which are: `BZIP2`, `DEFLATED`, `LZMA`

### Sample File Size

**ORIGINAL**|**BZIP2**|**DEFLATED**|**LZMA**
:-----:|:-----:|:-----:|:-----:
994K|735K|748K|751K
543K|402K|409K|412K
499K|369K|375K|377K
514K|381K|387K|390K
1200K|845K|861K|867K
484K|359K|364K|367K
617K|456K|463K|466K
967K|716K|730K|734K
1100K|831K|846K|852K
65K|49K|49K|50K
1300K|987K|1003K|1009K

### Compression Ratio

**BZIP2**|**DEFLATED**|**LZMA**
:-----:|:-----:|:-----:
74%|75%|76%
74%|75%|76%
74%|75%|76%
74%|75%|76%
70%|72%|72%
74%|75%|76%
74%|75%|76%
74%|75%|76%
76%|77%|77%
75%|75%|77%
76%|77%|78%

The performance in terms of compression ratio are roughly the same. 
This is expected, since the lragest part of file is the data of images, 
which are stored in `WebP` which have little to no room for compression.

The only high information entropy is mainly a result of using `Base64` encoding in json file. 
This can be confirmed by the roughly 75% compression ratio observed in all samples coupled with different algorithm.
(6 bits of data stored by a chracter, or a byte)

While `DEFLATED` does not provide the best compression ratio, 
it is much faster than other available methods. 
As its compression ratio is not far behind from other competitors, 
such as `BZIP`, `DEFLATED` as used for this project.

## Todo-list

- Better handle of error/exception, e.g. timed out exception
- Stickers preview for /popular
- New cache system (rename for LINE emoji)
  - MessagePack-based serialization
  - a file for each sticker set (can contain more than 30 stickers)
  - the actual json file is built upon request dynamically
    - key  : hash({tg/li/le/ka}-{name/name/id/name})
    - name
    - data
  - implementation of
    - /size
        n    : each set contains n stickers
        auto : stickers evenly distributed to sets
    - /cache rebuild
    rebuild the cache file
- Some site method will return None if failed, not all however
  - consistent output
  - add fail safe when returned object is None
- Purge the cache regularly
- implement avoidance of flood limit
  v12: https://github.com/python-telegram-bot/python-telegram-bot/wiki/Avoiding-flood-limits
  v11.1.0: https://github.com/python-telegram-bot/python-telegram-bot/wiki/Avoiding-flood-limits/bd30b9aa7c12515496af55650205485d3455821f
